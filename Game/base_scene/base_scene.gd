
extends Node2D

class LevelGenerator:
	var difficulty = 1
	var scene = null
	var scene_prototype = null
	var game = null
	var rects = []
	var view_rect = Rect2()
	var points = 0
	func _init(g, viewport_rect):
		game = g
		view_rect = viewport_rect
		scene_prototype = load("res://scene/scene_prototype.scn")
		var r = Rect2()
		r.pos = Vector2(0, 0)
		r.size = Vector2(view_rect.size.x/4, view_rect.size.y)
		rects.push_back(r)
		r.pos = Vector2(0, 0)
		r.size = Vector2(view_rect.size.x, view_rect.size.y/4)
		rects.push_back(r)
		r.pos = Vector2(0, view_rect.size.y-view_rect.size.y/4)
		r.size = Vector2(view_rect.size.x, view_rect.size.y/4)
		rects.push_back(r)
		r.pos = Vector2(view_rect.size.x-view_rect.size.x/4, 0)
		r.size = Vector2(view_rect.size.x/4, view_rect.size.y)
		rects.push_back(r)
	
	func generate(pos, rot, hp):
		if (game == null or scene_prototype == null):
			return
		scene = scene_prototype.instance()
		var sp = scene.get_node("player/spaceship")
		sp.set_pos(pos)
		sp.change_rot(rot)
		sp.hp = hp
		var diff = difficulty
		var pl_rect = which_rect(pos)
		generate_enemies(diff, pl_rect)
		scene.get_node("hud/points").set_text(str(points))
		scene.show()
		return scene
	
	func generate_from_hyperspace():
		if (game == null or scene_prototype == null):
			return
		var hp = scene.get_node("player/spaceship").hp
		var pos = generate_pos(randi()%4)
		var rot = rand_range(-2*PI, 2*PI)
		scene.queue_free()
		scene = scene_prototype.instance()
		var sp = scene.get_node("player/spaceship")
		sp.set_pos(pos)
		sp.change_rot(rot)
		sp.hp = hp
		var diff = difficulty
		generate_enemies(diff, -1)
		scene.get_node("hud/points").set_text(str(points))
		scene.show()
		return scene
	func generate_enemies(diff, pl_rect):
		var count
		var id = 0
		scene.enemies = []
		if (diff == 1):
			count = 1
			if (pl_rect == -1):
				add_enemy(1, id, pl_rect)
			else:
				add_enemy(1, id, pl_rect, Vector2(700, 60))
			id += 1
		else:
			count = randi()%diff+1
			while (count > 0):
				add_enemy(1, id, pl_rect)
				count -= 1
				id += 1
	
	func add_enemy(type, id, pl_rect, pos = Vector2(0,0)):
		if (type == 1):
			var en = scene.get_node("enemy").duplicate()
			en.show()
			scene.add_child(en)
			if (pos == Vector2(0,0)):
				if (pl_rect == -1):
					pos = generate_pos(randi()%4)
				else:
					pos = generate_pos(pl_rect)
			en.get_node("spaceship").set_pos(pos)
			var center = Vector2(view_rect.size.x/2, view_rect.size.y/2)
			var g_dir = center - pos
			var i = randi()%2
			var r
			if (i == 0):
				r = PI/2
			else:
				r = -PI/2
			var dir = g_dir.rotated(r).normalized()
			#keep this formula same as in spaceship.gd/_fixed_process()
			en.get_node("spaceship").speed = dir*30*0.625/(g_dir.length()/view_rect.size.x)
			scene.enemies.push_back(en)
			en.id = id
	
	func generate_pos(pl_rect):
		var r = pl_rect
		if (r == 1):
			r = 2
		elif (r == 2):
			r = 1
		elif (r == 0):
			r = 3
		elif (r == 3):
			r = 0
		var rect = rects[r]
		var pos = Vector2(rand_range(rect.pos.x, rect.size.x), rand_range(rect.pos.y, rect.size.y))
		return pos
	
	func which_rect(pos):
		for r in range(0, 4):
			if (rects[r].has_point(pos)):
				return r

var generator
var scene = null
var login_mode = true
var gj
var guest_name = ""
var points = 0 setget set_points
var username = ""
var game_token = ""
const game_id = "80016"
const pr_key = "56760772b82b3d19add38470a6b2fc85"
var show_controls_tip = true
var show_travel_tip = true
var show_hyperdrive_tip = true
var def_hp = 10
var rect

func _init():
	gj = GamejoltAPI.new()
	gj.init(game_id, pr_key)
	load_credentials()
	load_tip_settings()
	randomize()

func _exit_tree():
	if (!show_controls_tip or !show_hyperdrive_tip or !show_travel_tip):
		save_tip_settings()

func _ready():
	set_process_input(true)
	rect = get_viewport_rect()
	generator = LevelGenerator.new(self, rect)
	scene = generator.generate(Vector2(40, 700), 0, def_hp)
	if (username != "" and game_token != ""): #loaded
		get_node("main_menu/username").set_text(username)
		get_node("main_menu/game_token").set_secret(true)
		get_node("main_menu/game_token").set_text(game_token)

func lost():
	get_tree().set_pause(true)
	if (login_mode and points > 0):
		call_deferred("send_score", points)
	elif (guest_name != "" and points > 0):
		call_deferred("send_score", points, "", guest_name)
	scene.get_node("hud/quit").show()
	scene.get_node("hud/restart").show()

func _input(event):
	if (!scene.started):
		return
	if (event.is_action("pause") and !event.is_pressed()):
		if (get_tree().is_paused()):
			on_continue()
		else:
			pause()
	elif (event.is_action("clear_pause") and !event.is_pressed()):
		if (get_tree().is_paused()):
			get_tree().set_pause(false)
		else:
			get_tree().set_pause(true)

func won():
	generator.difficulty += 1
	travel_tip()
	show_controls_tip = false

func travel(pos, rot):
	var center = scene.center
	var hp = scene.get_node("player/spaceship").hp
	scene.queue_free()
	var new_pos = center-pos
	new_pos = include(rect, new_pos)
	scene = generator.generate(new_pos, rot, hp)
	add_child(scene)
	scene.start()
	show_travel_tip = false

func hyperspace_travel():
	scene.queue_free()
	scene = generator.generate_from_hyperspace()
	add_child(scene)
	scene.start()

func include(rect, new_pos):
	if (new_pos.x < rect.pos.x):
		new_pos.x = rect.pos.x+1
	elif (new_pos.x > rect.pos.x+rect.size.x):
		new_pos.x = rect.pos.x+rect.size.x-1
	if (new_pos.y < rect.pos.y):
		new_pos.y = rect.pos.y+1
	elif (new_pos.y > rect.pos.y+rect.size.y):
		new_pos.y = rect.pos.y+rect.size.y-1
	return new_pos

func _on_play_pressed():
	login_mode = false
	play()

func play():
	add_child(scene)
	get_node("main_menu").hide()
	get_node("title").hide()
	scene.start()


func _on_login_pressed():
	if (login_mode):
		username = get_node("main_menu/username").get_text()
		game_token = get_node("main_menu/game_token").get_text()
		if (!gj.login(username, game_token)):
			var message = gj.get_status_str()
			get_node("main_menu/messages").set_text(message)
			return
		else:
			save_credentials()
	else:
		guest_name = get_node("main_menu/username").get_text()
	play()

func _on_game_token_text_entered( text ):
	_on_login_pressed()

func _on_username_text_entered( text ):
	_on_login_pressed()

func _on_game_token_focus_enter():
	get_node("main_menu/game_token").set_text("")
	get_node("main_menu/game_token").set_secret(true)

func _on_username_focus_enter():
	get_node("main_menu/username").set_text("")

func _on_guest_pressed():
	if (login_mode):
		login_mode = false
		get_node("main_menu/login").set_text("Play as guest")
		get_node("main_menu/guest").set_text("Gamejolt login")
		get_node("main_menu/username").set_text("guest_name")
		get_node("main_menu/game_token").hide()
	else:
		login_mode = true
		get_node("main_menu/login").set_text("Login")
		get_node("main_menu/guest").set_text("Play as guest")
		get_node("main_menu/username").set_text("Username")
		get_node("main_menu/game_token").show()

func set_points(p):
	points = p
	generator.points = points
	if (scene != null):
		scene.get_node("hud/points").set_text(str(points))

func save_credentials():
	var f = File.new()
	var err = f.open_encrypted_with_pass("user://credentials.bin",File.WRITE,OS.get_unique_ID())
	f.store_var(username)
	f.store_var(game_token)
	f.close()

func load_credentials():
	var f = File.new()
	var err = f.open_encrypted_with_pass("user://credentials.bin", File.READ, OS.get_unique_ID())
	if (!f.is_open()):
		return
	username = f.get_var()
	game_token = f.get_var()
	f.close()

func send_score(points, scoreboard = "", guest_name = ""):
	message("Sending score..", 0.1, 0)
	var success = gj.send_score(points, scoreboard, guest_name)
	print("sending score")
	if (!success):
		message("Failed to send score: " + gj.get_status_str(), 10 ,false)
	else:
		message("Score sent", 10, false)

func message(string, time = 10, id = 0):
	if (scene != null):
		if (id == 0):
			scene.get_node("hud/messages").set_text(string)
		elif (id == 1):
			scene.get_node("hud/tips").set_text(string)
			scene.clear_messages(time) 

func controls_tip():
	if (!show_controls_tip):
		return
	message("Tip: Use mouse to aim, LMB(or 'S')-shoot, SPACEBAR-accelerate, 'H'-enter hyperspace", 10, 1)

func travel_tip():
	if (!show_travel_tip):
		return
	message("Tip: travel to the next level by going towards the edge of the screen", 10000, 1)

func hyperdrive_tip():
	if (!show_hyperdrive_tip):
		return
	message("Tip: enter hyperspace (button 'H') to avoid crashing", 100, 1)
	show_hyperdrive_tip = false

func save_tip_settings():
	var f = File.new()
	var err = f.open_encrypted_with_pass("user://data.bin",File.WRITE,OS.get_unique_ID())
	f.store_var(show_controls_tip)
	f.store_var(show_hyperdrive_tip)
	f.store_var(show_travel_tip)
	f.close()

func load_tip_settings():
	var f = File.new()
	var err = f.open_encrypted_with_pass("user://data.bin", File.READ, OS.get_unique_ID())
	if (!f.is_open()):
		return
	show_controls_tip = f.get_var()
	show_hyperdrive_tip = f.get_var()
	show_travel_tip = f.get_var()
	f.close()

func pause():
	if (scene == null):
		return
	get_tree().set_pause(true)
	scene.get_node("hud/continue").show()
	scene.get_node("hud/restart").show()
	scene.get_node("hud/quit").show()

func on_continue():
	if (scene == null):
		return
	scene.get_node("hud/continue").hide()
	scene.get_node("hud/restart").hide()
	scene.get_node("hud/quit").hide()
	get_tree().set_pause(false)

func on_quit():
	OS.get_main_loop().quit()

func on_restart():
	scene.queue_free()
	points = 0
	generator.points = points
	generator.difficulty = 1
	scene = generator.generate(Vector2(40, 700), 0, def_hp)
	get_tree().set_pause(false)
	play()
	

