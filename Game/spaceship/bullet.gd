
extends KinematicBody2D

var speed = 300*0.625
var facing_dir = Vector2(0, -1)
var vel = Vector2(0, 0)
var g_acc = 0
var center = Vector2(0, 0)
var damage = 1
var lifetime = 10
var time = 0
var bodies = []
var bullet_pull = 100
var id
var rect

func _ready():
	g_acc = get_parent().gravity
	center = get_parent().center
	rect = get_parent().get_parent().rect
	id = randf()

func shoot(from, dir):
	set_pos(from)
	var r = facing_dir.angle_to(dir)
	rotate(r)
	facing_dir = dir
	vel = dir.normalized()*speed
	set_fixed_process(true)

func _fixed_process(delta):
	collissions()
	time += delta
	if (time > lifetime):
		destroy()
	if (vel.length() == 0):
		return
	var dir = center - get_pos()
	vel += dir.normalized()*g_acc*delta/(dir.length()/rect.size.x)
	var r = facing_dir.angle_to(vel.normalized())
	rotate(r)
	facing_dir = vel.normalized()
	move(vel*delta)

func destroy():
	var prot = get_parent().get_node("explosion")
	var expl = prot.duplicate()
	expl.set_pos(get_pos())
	expl.show()
	get_parent().add_child(expl)
	expl.get_node("anim").play("small_explosion")
	expl.lifetime = 1
	queue_free()

func damage(damage):
	destroy()

func collissions():
	if (is_queued_for_deletion()):
		return
	if (is_colliding()):
		var obj = get_collider()
		if (obj != null and !obj.is_queued_for_deletion() and obj extends KinematicBody2D):
			obj.damage(damage)
		destroy()

func what():
	return "bullet"
