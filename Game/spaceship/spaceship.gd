
extends KinematicBody2D

var g_acc = Vector2()
var center = Vector2()
var scene
var acc = 30*0.625
var speed = Vector2(0,0)
var turn_speed = PI
var facing_dir = Vector2(0,-1)
var hp = 1
var engines = false setget set_engines
var turn = 0
var bullet_prot = null
var t_since_shot = 0
var shot_interval = 0.5
var flames = []
var name = "spaceship"
var hidden = false
var rect

func _ready():
	scene = get_parent().get_parent()
	g_acc = scene.gravity
	center = scene.center
	var rot = get_rot()
	facing_dir = facing_dir.rotated(rot)
	bullet_prot = scene.get_node("bullet")
	rect = get_parent().get_parent().get_parent().rect
	for ch in get_children():
		if (ch.get_name().substr(0, 6) == "flames"):
			flames.push_back(ch)
	set_process(true)

func _process(delta):
	pass
#	if (get_node("anims") != null and get_node("anims").is_playing()):
#		get_node("anims").advance(delta)

func _fixed_process(delta):
	collisions()
	facing_dir = Vector2(0, -1).rotated(get_rot())
	var pos = get_pos()
	var dir = center - pos
	#keep this the same as in base_scene.gd/add_enemy()
	speed += g_acc*delta*dir.normalized()/(dir.length()/rect.size.x)
	if (engines):
		speed += acc*delta*facing_dir
		turn_flames_on(true)
	else:
		turn_flames_on(false)
	if (abs(turn) > 0):
		var r = turn*turn_speed*delta
		if (abs(r) > abs(turn)):
			r = turn
			turn = 0
		else:
			turn -= r
		rotate(r)
	move(speed*delta)
	t_since_shot += delta

func collisions():
	if (is_queued_for_deletion() or hidden):
		return
	if (is_colliding()):
		var obj = get_collider()
		if (obj != null and !obj.is_queued_for_deletion() and obj extends KinematicBody2D):
			if (obj.what() == "spaceship"):
				obj.destroy()
				destroy()
			elif (obj.what() == "enemy_spaceship"):
				if (what() == "spaceship"):
					obj.destroy()
					destroy()
			elif (obj.what() == "bullet"):
				damage(obj.damage)
				obj.destroy()
		elif (obj != null and obj extends StaticBody2D):
			if (what() == "spaceship"):
				get_parent().get_parent().get_parent().hyperdrive_tip()
			destroy()

func destroy():
	var prot = get_parent().get_parent().get_node("explosion")
	var expl = prot.duplicate()
	expl.show()
	var pos = get_pos()
	get_parent().get_parent().add_child(expl)
	expl.set_pos(pos)
	expl.lifetime = 1
	if (is_queued_for_deletion() or hidden):
		return
	if (what() == "spaceship"):
		hp = 0
		get_parent().get_parent().update_health()
	get_parent().destroy()
	
func damage(points):
	if (hidden):
	 return
	hp -= points
	if (what() == "spaceship"):
		get_parent().get_parent().update_health()
	if (hp <= 0):
		destroy()

func turn_to(pos):
	turn = facing_dir.angle_to(pos - get_pos())

func shoot():
	if (t_since_shot < shot_interval):
		return
	var bullet = bullet_prot.duplicate()
	bullet.show()
	var pos = get_pos() + (get_node("sprite").get_texture().get_size().height+2)*facing_dir
	bullet.shoot(pos, facing_dir)
	scene.add_child(bullet)
	t_since_shot = 0

func what():
	return name

func change_rot(rot):
	rotate(rot)
	facing_dir = facing_dir.rotated(get_rot())

func turn_flames_on(enable):
	for fl in flames:
		if (enable):
			fl.show()
		else:
			fl.hide()

func set_engines(enable):
	if (enable == true):
		get_node("flames1/anims").play("engines_on")
		turn_flames_on(true)
	else:
		get_node("flames1/anims").stop()
		turn_flames_on(false)
	engines = enable