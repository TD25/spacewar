
extends Node2D

var center = Vector2()
var mouse_pos = Vector2()
var spaceship

func _ready():
	center = get_parent().center
	#set_process_input(true)
	spaceship = get_node("spaceship")

func destroy():
	get_parent().loose()
	queue_free()

func _fixed_process(delta):
	mouse_pos = get_viewport().get_mouse_pos()
	var cam_pos = get_parent().get_cam_offset()
	spaceship.turn_to(mouse_pos+cam_pos)
	if (Input.is_action_pressed("accelerate")):
		spaceship.engines = true
	else:
		spaceship.engines = false
	if (Input.is_action_pressed("shoot")):
		spaceship.shoot()
	if (Input.is_action_pressed("hyperspace")):
		spaceship.hide()
		spaceship.hidden = true
		get_parent().get_parent().hyperspace_travel()

func get_dir():
	var d = mouse_pos - spaceship.get_pos()
	return d.normalized()

func start():
	set_fixed_process(true)
	spaceship.set_fixed_process(true)



