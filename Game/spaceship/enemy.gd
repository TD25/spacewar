
extends Node2D

var center = Vector2()
var player
var spaceship
var min_dist = 250
var won = false
var id = 0
var shot_interval = 1.5
var t_since_shot = 0

func _ready():
	center = get_parent().center
	player = get_parent().get_node("player/spaceship")
	spaceship = get_node("spaceship")
	spaceship.name = "enemy_spaceship"

func _fixed_process(delta):
	t_since_shot += delta
	var pos = spaceship.get_pos()
	var distance = center - pos
	var dir = Vector2(0,0)
	if (!won):
		var en_dir = player.get_pos() - pos
		var t = spaceship.facing_dir.angle_to(en_dir)
		if (abs(t)< 0.1 and t_since_shot > shot_interval):
			spaceship.shoot()
			t_since_shot = 0
		else:
			spaceship.turn = t
#	if (dir.length() > 0):
#		spaceship.engines = true

func destroy():
	if (is_queued_for_deletion()):
		return
	get_parent().point(1, self)
	queue_free()

func start():
	set_fixed_process(true)
	spaceship.set_fixed_process(true)

func get_real_pos():
	return spaceship.get_pos()


