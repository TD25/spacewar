
extends Node2D

var gravity = 10*0.625
var center = Vector2()
var pl_spaceship
var enemies = []
var level_rect = Rect2()
var t_since_start = 0
var start_time = 5
var points
var max_zoom = 1.6
var min_zoom = 1
var clear_msg_in = -1
var started = false

func _init():
	enemies = []

func _enter_tree():
	var r = get_parent().rect
	center = Vector2(r.size.x/2, r.size.y/2)
	level_rect.pos = Vector2(-r.size.x, -r.size.y)
	level_rect.size = Vector2(r.size.x*3, r.size.y*3)

func _ready():
	pl_spaceship = get_node("player/spaceship")
	get_node("player").show()
	var s = enemies.size()
	points = get_parent().get_node("hud/points")
	update_health()

func start():
	set_fixed_process(true)
	set_process(true)
	get_node("player").start()
	get_parent().controls_tip()
	started = true
	for en in enemies:
		en.start()

func _process(delta):
	t_since_start += delta
	if (clear_msg_in > 0):
		clear_msg_in -= delta
	#fit player and star
	var pos = pl_spaceship.get_pos()
	var cam = get_node("Camera2D")
	var rect = get_parent().rect
	var cam_pos = cam.get_pos()
	var temp = 300
	var real_rect = rect
	rect.size.height -= temp
	rect.size.width -= temp
	real_rect.size.height -= temp
	real_rect.size.width -= temp
	var real_cam_pos = cam.get_pos()
	rect.pos = Vector2(cam_pos.x-rect.size.x/2, cam_pos.y-rect.size.y/2)
	real_rect.pos = Vector2(real_cam_pos.x-real_rect.size.x/2, real_cam_pos.y-real_rect.size.y/2)
	var to_en = dir_to_unseen_enemy(real_rect, real_cam_pos)
	if (!rect.has_point(pos)):
		var d = (pos - cam_pos).normalized()
		move_camera(d*(pl_spaceship.speed.length()+20)*delta)
	elif (to_en.length() > 0):
		var m = to_en.normalized()*200*delta
		move_camera(m)
		print("move to enemy"+str(m))
	elif (real_cam_pos != center):
		var m = pl_spaceship.speed.length()*delta*(center-cam_pos).normalized()
		move_camera(m)
		print("move to center" + str(m))
	if (!level_rect.has_point(pos)):
		get_parent().travel(pos, pl_spaceship.get_rot())
	if (clear_msg_in < 0):
		get_node("hud/tips").set_text("")
	#fit enemies
#	var zoom_out = true
#	var rect = get_view_rect()
#	for e in enemies:
#		if (e != null and rect.has_point(e.get_pos())):
#			zoom_out = false
#			break
#		elif (e == null):
#			zoom_out = false
#		else:
#			zoom_out = true
#	if (zoom_out and enemies.size() > 0 and cam.get_zoom().x < max_zoom):
#		var val = cam.get_zoom().x+0.0001
#		cam.set_zoom(Vector2(val, val))

func _fixed_process(delta):
	pass

func loose():
	for en in enemies:
		if (en != null):
			en.won = true
	call_deferred("lost")

func lost():
	get_parent().lost()

func point(p, inst):
	get_parent().points += p
	remove_enemy(inst)

func remove_enemy(inst):
	enemies.erase(inst)
	if (enemies.size() == 0):
		get_parent().won()

func get_cam_offset():
	var cam = get_node("Camera2D")
	var cam_pos = cam.get_pos()
	return cam_pos-center

func get_cam_center_offset():
	var pos = get_node("Camera2D").get_transform().get_origin()
	return pos

func center_camera():
	get_node("Camera2D").set_pos(center)

func move_camera(move):
	var c = get_node("Camera2D")
	c.set_pos(c.get_pos()+move)

func get_view_rect():
	var rect = get_parent().rect
	var cam = get_node("Camera2D")
	var new_rect = Rect2()
	new_rect.size = rect.size*cam.get_zoom().x
	new_rect.pos -= new_rect.size - rect.size
	return new_rect

func dir_to_unseen_enemy(rect, cam_pos):
	var d = Vector2(0, 0)
	var s = enemies.size()
	for en in enemies:
		if (en != null and rect.has_point(en.get_real_pos())):
			d = Vector2(0, 0)
			break
		elif (en != null):
			var dist = en.get_real_pos() - cam_pos
			if (dist.length() > d.length()):
				d = dist
	return d

func clear_messages(delay):
	clear_msg_in = delay

func update_health():
	var string = ""
	for i in range(0, pl_spaceship.hp):
		string += "i"
		if (pl_spaceship.hp <= 10):
			string += ' '
	get_node("hud/health").set_text(string)

func _on_continue_pressed():
	get_parent().on_continue()

func _on_quit_pressed():
	get_parent().on_quit()

func _on_restart_pressed():
	get_parent().call_deferred("on_restart")
