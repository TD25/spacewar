
extends Sprite

var lifetime = -1 setget set_lifetime#means a prototype
var time = 0

func _ready():
	pass

func set_lifetime(time):
	lifetime = time
	set_process(true)

func _process(delta):
	time += delta
	if (time > lifetime):
		queue_free()


